package com.diningPhilosophers.tests;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import com.diningPhilosophers.bean.Chopstick;
import com.diningPhilosophers.bean.Philosopher;
import com.diningPhilosophers.gui.GraphicTable;

public class TestsDiningPhilosophers {

    @Test
    public void testDeadLock() throws InterruptedException{
        GraphicTable table = new GraphicTable();
        
        Chopstick chopstick1 = new Chopstick(0);
        Chopstick chopstick2 = new Chopstick(4);
        Chopstick chopstick3 = new Chopstick(1);
        Chopstick chopstick4 = new Chopstick(0);
        
        Philosopher philosopher1 = new Philosopher(table, chopstick1, chopstick2);
        Philosopher philosopher2 = new Philosopher(table, chopstick3, chopstick4);
        
        Thread t1= new Thread( philosopher1 );
        Thread t2= new Thread( philosopher2 );
        t1.start();
        t2.start();
    }

}
