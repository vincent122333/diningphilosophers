package com.diningPhilosophers.bean;
import com.diningPhilosophers.gui.GraphicTable;
import com.diningPhilosophers.utils.Log;
import com.diningPhilosophers.utils.PhilosophersConstants;

/**
 * @author Vincent Perrault
 * Classe d'un philosophe qui veux manger sur la table
 */
public class Philosopher extends Thread implements PhilosophersConstants{
    
    private static int idCounter = 0;
    private int id;
    
    private Chopstick leftChopstick;
    private Chopstick rightChopstick;
    
    private GraphicTable table;

    /**
     * Contructeur
     * @param table			Table du philosophr
     * @param leftChopstick	Ustensile gauche
     * @param rightChopstick	Ustensile droite
     */
    public Philosopher(GraphicTable table, Chopstick leftChopstick, Chopstick rightChopstick) {
    	super(FIRST_NAME + Philosopher.idCounter);
    	this.id = Philosopher.idCounter++;
        this.table = table;
        this.leftChopstick = leftChopstick;
        this.rightChopstick = rightChopstick;
    }

    /**
     * �xecute un Thread, le philosophe qui vient manger � une table
     */
    public void run() {
    	this.doSleep(TIME_SLEEP_INIT);
    	for (; ; ){
    	    beginProcess();
    	}
    }
    
    public void beginProcess()
    {
        this.think();
        this.becomesHungry();
        if(! leftChopstick.isUsed()){
            if(!rightChopstick.isUsed()){
                this.takeChopstick(this.leftChopstick, "got left chopstick");
                this.doSleep(TIME_NEXT_FORK);
                this.takeChopstick(this.rightChopstick, "got right chopstick");
                
                this.eat();
                
                this.releaseChopstick(this.leftChopstick, "released left chopstick");
                this.releaseChopstick(this.rightChopstick, "released right chopstick");
            }
        }      
    }
    
    public void think(){
        Log.imprimerMessageConsole (super.getName() + " thinks");
        this.table.doThinking(this.id);
        this.doSleep((long)(Math.random()*TIME_THINK_MAX));
        Log.imprimerMessageConsole (getName() + " finished thinking");
    }
    
    private void becomesHungry() {
        Log.imprimerMessageConsole (getName() + " is hungry");
        this.table.becomesHungry(this.id);
    }
    
    /**
     * Prend un ustensile
     * @param chopstick    L'ustensil � prendre
     * @param message      Message significatif � imprimer
     */
    private void takeChopstick(Chopstick chopstick, String message) {
        Log.imprimerMessageConsole (this.getName() + " " + message);
        this.table.take(chopstick);
        this.table.takeChopstick(this.id, chopstick);
    }
    
    /**
     * Rel�che un ustensile
     * @param chopstick_to_release    L'ustensile � l�cher
     * @param pMessage      Message significatif
     */
    private void releaseChopstick(Chopstick chopstick_to_release, String pMessage) {
        this.table.releaseChopStick(chopstick_to_release);
        Log.imprimerMessageConsole (this.getName() + " " + pMessage);   
    }

    /**
     * Fait manger le philosophe
     */
    private void eat() {
        final double timeToEat = Math.random() * TIME_EAT_MAX;
        Log.imprimerMessageConsole (this.getName() + " eats for " + timeToEat);
        this.doSleep((long) timeToEat);
        Log.imprimerMessageConsole (this.getName() + " finished eating");
    }
    
	/**
	 * Fait attendre le philosophe
	 * @param temps_milis	Temps d'attente (en millisecondes)
	 */
    private void doSleep(long temps_milis) {
        try {
            Thread.sleep(temps_milis);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

}
