package com.diningPhilosophers.bean;

import com.diningPhilosophers.utils.Log;

public class Chopstick {
    
    private int id;
    private boolean used;

    public Chopstick(int id_chopstick){
        this.id = id_chopstick;
        this.used = false;
    }

    public int getId() {
        return id;
    }

    public boolean isUsed() {
        return used;
    }

    public synchronized void take() {
        Log.imprimerMessageConsole ("Used :: " + id );
        this.used = true;
    }
    public synchronized void release() {
        Log.imprimerMessageConsole ("Released :: " + id );
        this.used = false;
    }
}
