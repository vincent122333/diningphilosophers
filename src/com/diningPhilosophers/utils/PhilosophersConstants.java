package com.diningPhilosophers.utils;

public interface PhilosophersConstants {
    
    static final int TIME_THINK_MAX = 4000;//4000;
    static final int TIME_NEXT_FORK = 1000;//1000;
    static final int TIME_EAT_MAX = 3000;//3000;
    static final String FIRST_NAME = "Philosoper ";
    static final long TIME_SLEEP_INIT = 20; // 5001
}
