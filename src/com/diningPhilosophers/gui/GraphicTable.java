package com.diningPhilosophers.gui;

import java.awt.*;
import java.awt.event.*;

import com.diningPhilosophers.bean.Chopstick;
import com.diningPhilosophers.bean.Philosopher;
import com.diningPhilosophers.utils.Log;

public class GraphicTable extends Frame implements WindowListener {
    public static final int MAX = 5;
    static private int compte = 0;
    static private int enAttente = 0;
    private Point center;
    private GraphicPlate plates[];
    private GraphicChopstick chops[];
    private Chopstick chopsticks[];

    public GraphicTable() {
        super();
        
        chopsticks = new Chopstick[5];

        //initlize the chopistics
        for(int i=0; i< chopsticks.length; i++){
            chopsticks[i] = new Chopstick(i);
        }
        addWindowListener(this);
        setTitle("Dining Philosophers");
        setSize(200, 200);
        setBackground(Color.darkGray);

        center = new Point(getSize().width / 2, getSize().height / 2);

        plates = new GraphicPlate[5];
        for (int i = 0; i < 5; i++) {
            plates[i] = new GraphicPlate(i, center, new Point(center.x, center.y - 70), 20);
        }

        chops = new GraphicChopstick[5];
        for (int i = 0; i < 5; i++) {
            chops[i] = new GraphicChopstick(i, center,
                    new Point(center.x, center.y - 70),
                    new Point(center.x, center.y - 40));
        }

        show();
        setResizable(false);

        Philosopher[] philosophers = new Philosopher[5];
        //for(i=0; i<philosophers.length; i++){
        philosophers[0] = new Philosopher(this, chopsticks[0], chopsticks[4]);
        philosophers[1] = new Philosopher(this, chopsticks[1], chopsticks[0]);
        philosophers[2] = new Philosopher(this, chopsticks[2], chopsticks[1]);
        philosophers[3] = new Philosopher(this, chopsticks[3], chopsticks[2]);
        philosophers[4] = new Philosopher(this, chopsticks[4], chopsticks[3]);

        for(int i=0;i<philosophers.length;i++){
            Log.imprimerMessageConsole ("Thred "+ i);
            Thread t= new Thread( philosophers[i]);
            t.start();
        }

    }

    public void windowOpened(WindowEvent evt) {
    }

    public void windowClosing(WindowEvent evt) {
        System.exit(0);
    }

    public void windowClosed(WindowEvent evt) {
    }

    public void windowIconified(WindowEvent evt) {
    }

    public void windowDeiconified(WindowEvent evt) {
    }

    public void windowActivated(WindowEvent evt) {
    }

    public void windowDeactivated(WindowEvent evt) {
    }

    public synchronized void becomesHungry(int phID) {
        while (compte == MAX || enAttente > 0) {
            attente();
        }
        compte++;
        plates[phID].setColor(phID);
        repaint();
    }

    private void attente() {
        try {
            enAttente++;
            wait();
            enAttente--;
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }

    public synchronized void doThinking(int phID) {
        plates[phID].setColor(-1);
        repaint();
        compte--;
        notify();
    }

    public synchronized void take(Chopstick chopstick_taked) {
        chopsticks[chopstick_taked.getId()].take();
        notifyAll();
    }
    
    public void takeChopstick(int phID, Chopstick chopstick_selected) {
        System.out.println(String.format("repainting ph %d - color %d ", phID, chopstick_selected.getId()));
        chops[chopstick_selected.getId()].setColor(phID);
        repaint();
    }

    public synchronized void releaseChopStick(Chopstick chopstick_to_release) {
        chopsticks[chopstick_to_release.getId()].release();
        chops[chopstick_to_release.getId()].setColor(-1);
        repaint();
        notifyAll();
    }

    public void paint(Graphics g) {
        for (int i = 0; i < 5; i++) {
            plates[i].draw(g);
            chops[i].draw(g);
        }
    }
}
